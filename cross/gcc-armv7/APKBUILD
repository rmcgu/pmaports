# Automatically generated aport, do not edit!
# Generator: pmbootstrap aportgen gcc-armv7
# Based on: main/gcc (from Alpine)

CTARGET_ARCH=armv7
CTARGET="$(arch_to_hostspec ${CTARGET_ARCH})"
LANG_D=false
LANG_OBJC=false
LANG_JAVA=false
LANG_GO=false
LANG_FORTRAN=false
LANG_ADA=false
options="!strip !tracedeps"

# abuild doesn't try to tries to install "build-base-$CTARGET_ARCH"
# when this variable matches "no*"
BOOTSTRAP="nobuildbase"

# abuild will only cross compile when this variable is set, but it
# needs to find a valid package database in there for dependency
# resolving, so we set it to /.
CBUILDROOT="/"

_cross_configure="--disable-bootstrap --with-sysroot=/usr/$CTARGET"

pkgname=gcc-armv7
pkgver=10.2.0
[ "$BOOTSTRAP" = "nolibc" ] && pkgname="gcc-pass2"
[ "$CBUILD" != "$CHOST" ] && _cross="-$CARCH" || _cross=""
[ "$CHOST" != "$CTARGET" ] && _target="-$CTARGET_ARCH" || _target=""

pkgname=gcc-armv7
pkgrel=1
pkgdesc="Stage2 cross-compiler for armv7"
url="https://gcc.gnu.org"
arch="x86_64"
license="GPL-2.0-or-later LGPL-2.1-or-later"
_gccrel=$pkgver-r$pkgrel
depends="isl binutils-armv7 mpc1"
makedepends_build="gcc g++ paxmark bison flex texinfo gawk zip gmp-dev mpfr-dev mpc1-dev zlib-dev"
makedepends_host="linux-headers gmp-dev mpfr-dev mpc1-dev isl-dev zlib-dev musl-dev-armv7 binutils-armv7"
subpackages="g++-armv7:gpp"
[ "$CHOST" = "$CTARGET" ] && subpackages="gcc-doc$_target"
replaces="libstdc++ binutils"

: "${LANG_CXX:=true}"
: "${LANG_D:=true}"
: "${LANG_OBJC:=true}"
: "${LANG_GO:=true}"
: "${LANG_FORTRAN:=true}"
: "${LANG_ADA:=true}"

_libgomp=true
_libgcc=true
_libatomic=true
_libitm=true

if [ "$CHOST" != "$CTARGET" ]; then
	if [ "$BOOTSTRAP" = nolibc ]; then
		LANG_CXX=false
		LANG_ADA=false
		_libgcc=false
		_builddir="$srcdir/build-cross-pass2"
	else
		_builddir="$srcdir/build-cross-final"
	fi
	LANG_OBJC=false
	LANG_GO=false
	LANG_FORTRAN=false
	LANG_D=false
	_libgomp=false
	_libatomic=false
	_libitm=false

	# reset target flags (should be set in crosscreate abuild)
	# fixup flags. seems gcc treats CPPFLAGS as global without
	# _FOR_xxx variants. wrap it in CFLAGS and CXXFLAGS.
	export CFLAGS="$CPPFLAGS $CFLAGS"
	export CXXFLAGS="$CPPFLAGS $CXXFLAGS"
	unset CPPFLAGS
	export CFLAGS_FOR_TARGET=" "
	export CXXFLAGS_FOR_TARGET=" "
	export LDFLAGS_FOR_TARGET=" "

	STRIP_FOR_TARGET="$CTARGET-strip"
elif [ "$CBUILD" != "$CHOST" ]; then
	# fixup flags. seems gcc treats CPPFLAGS as global without
	# _FOR_xxx variants. wrap it in CFLAGS and CXXFLAGS.
	export CFLAGS="$CPPFLAGS $CFLAGS"
	export CXXFLAGS="$CPPFLAGS $CXXFLAGS"
	unset CPPFLAGS

	# reset flags and cc for build
	export CC_FOR_BUILD="gcc"
	export CXX_FOR_BUILD="g++"
	export CFLAGS_FOR_BUILD=" "
	export CXXFLAGS_FOR_BUILD=" "
	export LDFLAGS_FOR_BUILD=" "
	export CFLAGS_FOR_TARGET=" "
	export CXXFLAGS_FOR_TARGET=" "
	export LDFLAGS_FOR_TARGET=" "

	# Languages that do not need bootstrapping
	LANG_OBJC=false
	LANG_GO=false
	LANG_FORTRAN=false
	LANG_D=false

	STRIP_FOR_TARGET=${CROSS_COMPILE}strip
	_builddir="$srcdir/build-cross-native"
else
	STRIP_FOR_TARGET=${CROSS_COMPILE}strip
	_builddir="$srcdir/build"
fi

# GDC hasn't been ported to PowerPC
# See libphobos/configure.tgt in GCC sources for supported targets
[ "$CARCH" = ppc64le ] && LANG_D=false

# Go needs {set,make,swap}context, unimplemented in musl
[ "$CTARGET_LIBC" = musl ] && LANG_GO=false

# libitm has TEXTRELs in ARM build, so disable for now
case "$CTARGET_ARCH" in
arm*)		_libitm=false ;;
mips*)		_libitm=false ;;
esac

# Fortran uses libquadmath if toolchain has __float128
# currently on x86, x86_64 and ia64
_libquadmath=$LANG_FORTRAN
case "$CTARGET_ARCH" in
x86 | x86_64)	_libquadmath=$LANG_FORTRAN ;;
*)		_libquadmath=false ;;
esac

# libatomic is a dependency for openvswitch
$_libatomic && subpackages="$subpackages libatomic::$CTARGET_ARCH"
$_libgcc && subpackages="$subpackages libgcc::$CTARGET_ARCH"
$_libquadmath && subpackages="$subpackages libquadmath::$CTARGET_ARCH"
if $_libgomp; then
	depends="$depends libgomp=$_gccrel"
	subpackages="$subpackages libgomp::$CTARGET_ARCH"
fi

_languages=c
if $LANG_CXX; then
	_languages="$_languages,c++"
fi
if $LANG_D; then
	subpackages="$subpackages libgphobos::$CTARGET_ARCH gcc-gdc$_target:gdc"
	_languages="$_languages,d"
	makedepends_build="$makedepends_build libucontext-dev"
fi
if $LANG_OBJC; then
	subpackages="$subpackages libobjc::$CTARGET_ARCH gcc-objc$_target:objc"
	_languages="$_languages,objc"
fi
if $LANG_GO; then
	subpackages="$subpackages libgo::$CTARGET_ARCH gcc-go$_target:go"
	_languages="$_languages,go"
fi
if $LANG_FORTRAN; then
	subpackages="$subpackages libgfortran::$CTARGET_ARCH gfortran$_target:gfortran"
	_languages="$_languages,fortran"
fi
if $LANG_ADA; then
	subpackages="$subpackages libgnat::$CTARGET_ARCH gcc-gnat$_target:gnat"
	_languages="$_languages,ada"
	[ "$CBUILD" = "$CTARGET" ] && makedepends_build="$makedepends_build gcc-gnat-bootstrap"
	[ "$CBUILD" != "$CTARGET" ] && makedepends_build="$makedepends_build gcc-gnat gcc-gnat$_cross"
fi
makedepends="$makedepends_build $makedepends_host"

source="https://gcc.gnu.org/pub/gcc/releases/gcc-${_pkgbase:-$pkgver}/gcc-${_pkgbase:-$pkgver}.tar.xz
	0001-posix_memalign.patch
	0002-gcc-poison-system-directories.patch
	0003-Turn-on-Wl-z-relro-z-now-by-default.patch
	0004-Turn-on-D_FORTIFY_SOURCE-2-by-default-for-C-C-ObjC-O.patch
	0005-On-linux-targets-pass-as-needed-by-default-to-the-li.patch
	0006-Enable-Wformat-and-Wformat-security-by-default.patch
	0007-Enable-Wtrampolines-by-default.patch
	0008-Disable-ssp-on-nostdlib-nodefaultlibs-and-ffreestand.patch
	0009-Ensure-that-msgfmt-doesn-t-encounter-problems-during.patch
	0010-Don-t-declare-asprintf-if-defined-as-a-macro.patch
	0011-libiberty-copy-PIC-objects-during-build-process.patch
	0012-libitm-disable-FORTIFY.patch
	0013-libgcc_s.patch
	0014-nopie.patch
	0015-libffi-use-__linux__-instead-of-__gnu_linux__-for-mu.patch
	0016-dlang-update-zlib-binding.patch
	0017-dlang-fix-fcntl-on-mips-add-libucontext-dep.patch
	0018-ada-fix-shared-linking.patch
	0019-build-fix-CXXFLAGS_FOR_BUILD-passing.patch
	0020-libstdc-futex-add-time64-compatibility.patch
	0021-add-fortify-headers-paths.patch
	0022-Alpine-musl-package-provides-libssp_nonshared.a.-We-.patch
	0023-DP-Use-push-state-pop-state-for-gold-as-well-when-li.patch
	0024-Pure-64-bit-MIPS.patch
	0025-use-pure-64-bit-configuration-where-appropriate.patch
	0026-always-build-libgcc_eh.a.patch
	0027-ada-libgnarl-compatibility-for-musl.patch
	0028-ada-musl-support-fixes.patch
	"

#	gcc-4.8-build-args.patch

# we build out-of-tree
_gccdir="$srcdir"/gcc-${_pkgbase:-$pkgver}
_gcclibdir="/usr/lib/gcc/$CTARGET/$pkgver"
_gcclibexec="/usr/libexec/gcc/$CTARGET/$pkgver"

prepare() {
	cd "$_gccdir"

	_err=
	for i in $source; do
		case "$i" in
		*.patch)
			msg "Applying $i"
			patch -p1 -i "$srcdir"/$i || _err="$_err $i"
			;;
		esac
	done

	if [ -n "$_err" ]; then
		error "The following patches failed:"
		for i in $_err; do
			echo "  $i"
		done
		return 1
	fi

	echo ${pkgver} > gcc/BASE-VER
}

build() {
	local _arch_configure=
	local _libc_configure=
	local _bootstrap_configure=
	local _symvers=

	cd "$_gccdir"

	case "$CTARGET" in
	aarch64-*-*-*)		_arch_configure="--with-arch=armv8-a --with-abi=lp64";;
	armv5-*-*-*eabi)	_arch_configure="--with-arch=armv5te --with-tune=arm926ej-s --with-float=soft --with-abi=aapcs-linux";;
	armv6-*-*-*eabihf)	_arch_configure="--with-arch=armv6zk --with-tune=arm1176jzf-s --with-fpu=vfp --with-float=hard --with-abi=aapcs-linux";;
	armv7-*-*-*eabihf)	_arch_configure="--with-arch=armv7-a --with-tune=generic-armv7-a --with-fpu=vfpv3-d16 --with-float=hard --with-abi=aapcs-linux --with-mode=thumb";;
	mips-*-*-*)		_arch_configure="--with-arch=mips32 --with-mips-plt --with-float=soft --with-abi=32";;
	mips64-*-*-*)		_arch_configure="--with-arch=mips3 --with-tune=mips64 --with-mips-plt --with-float=soft --with-abi=64";;
	mips64el-*-*-*)		_arch_configure="--with-arch=mips3 --with-tune=mips64 --with-mips-plt --with-float=soft --with-abi=64";;
	mipsel-*-*-*)		_arch_configure="--with-arch=mips32 --with-mips-plt --with-float=soft --with-abi=32";;
	powerpc-*-*-*)		_arch_configure="--enable-secureplt --enable-decimal-float=no";;
	powerpc64*-*-*-*)	_arch_configure="--with-abi=elfv2 --enable-secureplt --enable-decimal-float=no --enable-targets=powerpcle-linux";;
	i486-*-*-*)		_arch_configure="--with-arch=i486 --with-tune=generic --enable-cld";;
	i586-*-*-*)		_arch_configure="--with-arch=i586 --with-tune=generic --enable-cld";;
	s390x-*-*-*)		_arch_configure="--with-arch=z196 --with-tune=zEC12 --with-zarch --with-long-double-128 --enable-decimal-float";;
	esac

	case "$CTARGET_ARCH" in
	mips*)	_hash_style_configure="--with-linker-hash-style=sysv" ;;
	*)	_hash_style_configure="--with-linker-hash-style=gnu" ;;
	esac

	case "$CTARGET_LIBC" in
	musl)
		# musl does not support mudflap, or libsanitizer
		# libmpx uses secure_getenv and struct _libc_fpstate not present in musl
		# alpine musl provides libssp_nonshared.a, so we don't need libssp either
		_libc_configure="--disable-libssp --disable-libmpx --disable-libmudflap --disable-libsanitizer"
		_symvers="--disable-symvers"
		export libat_cv_have_ifunc=no
		;;
	esac


	case "$BOOTSTRAP" in
	nolibc)	_bootstrap_configure="--with-newlib --disable-shared --enable-threads=no" ;;
	*)	_bootstrap_configure="--enable-shared --enable-threads --enable-tls" ;;
	esac

	$_libgomp	|| _bootstrap_configure="$_bootstrap_configure --disable-libgomp"
	$_libatomic	|| _bootstrap_configure="$_bootstrap_configure --disable-libatomic"
	$_libitm	|| _bootstrap_configure="$_bootstrap_configure --disable-libitm"
	$_libquadmath	|| _arch_configure="$_arch_configure --disable-libquadmath"

	msg "Building the following:"
	echo ""
	echo "  CBUILD=$CBUILD"
	echo "  CHOST=$CHOST"
	echo "  CTARGET=$CTARGET"
	echo "  CTARGET_ARCH=$CTARGET_ARCH"
	echo "  CTARGET_LIBC=$CTARGET_LIBC"
	echo "  languages=$_languages"
	echo "  arch_configure=$_arch_configure"
	echo "  libc_configure=$_libc_configure"
	echo "  cross_configure=$_cross_configure"
	echo "  bootstrap_configure=$_bootstrap_configure"
	echo "	hash_style_configure=$_hash_style_configure"
	echo ""

	export CFLAGS="$CFLAGS -O2"

	mkdir -p "$_builddir"
	cd "$_builddir"
	"$_gccdir"/configure --prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--build=${CBUILD} \
		--host=${CHOST} \
		--target=${CTARGET} \
		--with-pkgversion="Alpine $pkgver" \
		--enable-checking=release \
		--disable-fixed-point \
		--disable-libstdcxx-pch \
		--disable-multilib \
		--disable-nls \
		--disable-werror \
		$_symvers \
		--enable-__cxa_atexit \
		--enable-default-pie \
		--enable-default-ssp \
		--enable-cloog-backend \
		--enable-languages=$_languages \
		$_arch_configure \
		$_libc_configure \
		$_cross_configure \
		$_bootstrap_configure \
		--with-system-zlib \
		$_hash_style_configure
	make
}

package() {
	cd "$_builddir"
	make -j1 DESTDIR="$pkgdir" install

	ln -s gcc "$pkgdir"/usr/bin/cc

	# we dont support gcj -static
	# and saving 35MB is not bad.
	find "$pkgdir" \( -name libgtkpeer.a \
		-o -name libgjsmalsa.a \
		-o -name libgij.a \) \
		-delete

	# strip debug info from some static libs
	find "$pkgdir" \( -name libgfortran.a -o -name libobjc.a -o -name libgomp.a \
		-o -name libgphobos.a -o -name libgdruntime.a \
		-o -name libmudflap.a -o -name libmudflapth.a \
		-o -name libgcc.a -o -name libgcov.a -o -name libquadmath.a \
		-o -name libitm.a -o -name libgo.a -o -name libcaf\*.a \
		-o -name libatomic.a -o -name libasan.a -o -name libtsan.a \) \
		-a -type f \
		-exec ${STRIP_FOR_TARGET} -g {} +

	if $_libgomp; then
		mv "$pkgdir"/usr/lib/libgomp.spec "$pkgdir"/$_gcclibdir
	fi
	if $_libitm; then
		mv "$pkgdir"/usr/lib/libitm.spec "$pkgdir"/$_gcclibdir
	fi

	# remove ffi
	rm -f "$pkgdir"/usr/lib/libffi* "$pkgdir"/usr/share/man/man3/ffi*
	find "$pkgdir" -name 'ffi*.h' -delete

	local gdblib=${_target:+$CTARGET/}lib
	if [ -d "$pkgdir"/usr/$gdblib/ ]; then
		for i in $(find "$pkgdir"/usr/$gdblib/ -type f -maxdepth 1 -name "*-gdb.py"); do
			mkdir -p "$pkgdir"/usr/share/gdb/python/auto-load/usr/$gdblib
			mv "$i" "$pkgdir"/usr/share/gdb/python/auto-load/usr/$gdblib/
		done
	fi

	paxmark -pmrs "$pkgdir"/$_gcclibexec/cc1

	# move ada runtime libs
	if $LANG_ADA; then
		for i in $(find "$pkgdir"/$_gcclibdir/adalib/ -type f -maxdepth 1 -name "libgna*.so"); do
			mv "$i" "$pkgdir"/usr/lib/
			ln -s ../../../../${i##*/} $i
		done
	fi

	if [ "$CHOST" != "$CTARGET" ]; then
		# cross-gcc: remove any files that would conflict with the
		# native gcc package
		rm -rf "$pkgdir"/usr/bin/cc "$pkgdir"/usr/include "${pkgdir:?}"/usr/share
		# libcc1 does not depend on target, don't ship it
		rm -rf "$pkgdir"/usr/lib/libcc1.so*

		# fixup gcc library symlinks to be linker scripts so
		# linker finds the libs from relocated sysroot
		for so in "$pkgdir"/usr/"$CTARGET"/lib/*.so; do
			if [ -h "$so" ]; then
				local _real=$(basename "$(readlink "$so")")
				rm -f "$so"
				echo "GROUP ($_real)" > "$so"
			fi
		done
	else
		# add c89/c99 wrapper scripts
		cat >"$pkgdir"/usr/bin/c89 <<'EOF'
#!/bin/sh
_flavor="-std=c89"
for opt; do
	case "$opt" in
	-ansi|-std=c89|-std=iso9899:1990) _flavor="";;
	-std=*) echo "$(basename $0) called with non ANSI/ISO C option $opt" >&2
		exit 1;;
	esac
done
exec gcc $_flavor ${1+"$@"}
EOF
		cat >"$pkgdir"/usr/bin/c99 <<'EOF'
#!/bin/sh
_flavor="-std=c99"
for opt; do
	case "$opt" in
	-std=c99|-std=iso9899:1999) _flavor="";;
	-std=*) echo "$(basename $0) called with non ISO C99 option $opt" >&2
		exit 1;;
	esac
done
exec gcc $_flavor ${1+"$@"}
EOF
		chmod 755 "$pkgdir"/usr/bin/c?9

		# install lto plugin so regular binutils may use it
		mkdir -p "$pkgdir"/usr/lib/bfd-plugins
		ln -s /$_gcclibexec/liblto_plugin.so "$pkgdir/usr/lib/bfd-plugins/"
	fi
}

libatomic() {
	pkgdesc="GCC Atomic library"
	depends=
	replaces="gcc"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libatomic.so.* "$subpkgdir"/usr/lib/
}

libcxx() {
	pkgdesc="GNU C++ standard runtime library"
	depends=

	if [ "$CHOST" = "$CTARGET" ]; then
		# verify that we are using clock_gettime rather than doing direct syscalls
		# so we dont break 32 bit arches due to time64.
		nm -D "$pkgdir"/usr/lib/libstdc++.so.* | grep clock_gettime
	fi

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libstdc++.so.* "$subpkgdir"/usr/lib/
}

gpp() {
	pkgdesc="GNU C++ standard library and compiler"
	depends="libstdc++=$_gccrel gcc=$_gccrel libc-dev"
	mkdir -p "$subpkgdir/$_gcclibexec" \
		"$subpkgdir"/usr/bin \
		"$subpkgdir"/usr/${_target:+$CTARGET/}include \
		"$subpkgdir"/usr/${_target:+$CTARGET/}lib \

	mv "$pkgdir/$_gcclibexec/cc1plus" "$subpkgdir/$_gcclibexec/"
	paxmark -pmrs "$subpkgdir/$_gcclibexec/cc1plus"

	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/*++* "$subpkgdir"/usr/${_target:+$CTARGET/}lib/
	mv "$pkgdir"/usr/${_target:+$CTARGET/}include/c++ "$subpkgdir"/usr/${_target:+$CTARGET/}include/
	mv "$pkgdir"/usr/bin/*++ "$subpkgdir"/usr/bin/
}

libobjc() {
	pkgdesc="GNU Objective-C runtime"
	replaces="objc"
	depends=
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libobjc.so.* "$subpkgdir"/usr/lib/
}

objc() {
	pkgdesc="GNU Objective-C"
	replaces="gcc"
	depends="libc-dev gcc=$_gccrel libobjc=$_gccrel"

	mkdir -p "$subpkgdir/$_gcclibexec" \
		"$subpkgdir"/$_gcclibdir/include \
		"$subpkgdir"/usr/lib
	mv "$pkgdir/$_gcclibexec/cc1obj" "$subpkgdir/$_gcclibexec/"
	mv "$pkgdir"/$_gcclibdir/include/objc "$subpkgdir"/$_gcclibdir/include/
	mv "$pkgdir"/usr/lib/libobjc.so "$pkgdir"/usr/lib/libobjc.a \
		"$subpkgdir"/usr/lib/
}

libgcc() {
	pkgdesc="GNU C compiler runtime libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libgcc_s.so.* "$subpkgdir"/usr/lib/
}

libgomp() {
	pkgdesc="GCC shared-memory parallel programming API library"
	depends=
	replaces="gcc"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libgomp.so.* "$subpkgdir"/usr/lib/
}

libgphobos() {
	pkgdesc="D programming language standard library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgdruntime.so.* "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.so.*  "$subpkgdir"/usr/lib/
}

gdc() {
	pkgdesc="GCC-based D language compiler"
	depends="gcc=$_gccrel libgphobos=$_gccrel musl-dev"
	depends="$depends libucontext-dev"

	mkdir -p "$subpkgdir/$_gcclibexec" \
		"$subpkgdir"/$_gcclibdir/include/d/ \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	# Copy: The installed '.d' files, the static lib, the binary itself
	# The shared libs are part of 'libgphobos' so one can run program
	# without installing the compiler
	mv "$pkgdir/$_gcclibexec/d21" "$subpkgdir/$_gcclibexec/"
	mv "$pkgdir"/$_gcclibdir/include/d/* "$subpkgdir"/$_gcclibdir/include/d/
	mv "$pkgdir"/usr/lib/libgdruntime.a "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.a "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.spec "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/bin/$CTARGET-gdc "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/bin/gdc "$subpkgdir"/usr/bin/
}


libgo() {
	pkgdesc="Go runtime library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgo.so.* "$subpkgdir"/usr/lib/
}

go() {
	pkgdesc="Go support for GCC"
	depends="gcc=$_gccrel libgo=$_gccrel"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/lib/go "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/bin/*gccgo "$subpkgdir"/usr/bin/
	mv "$pkgdir"/$_gcclibexec/go1 "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/usr/lib/libgo.a \
		"$pkgdir"/usr/lib/libgo.so \
		"$pkgdir"/usr/lib/libgobegin.a \
		"$subpkgdir"/usr/lib/
}

libgfortran() {
	pkgdesc="Fortran runtime library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgfortran.so.* "$subpkgdir"/usr/lib/
}

libquadmath() {
	replaces="gcc"
	pkgdesc="128-bit math library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libquadmath.so.* "$subpkgdir"/usr/lib/
}

gfortran() {
	pkgdesc="GNU Fortran Compiler"
	depends="gcc=$_gccrel libgfortran=$_gccrel"
	$_libquadmath && depends="$depends libquadmath=$_gccrel"
	replaces="gcc"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/$_gcclibdir \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/*gfortran "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/lib/libgfortran.a \
		"$pkgdir"/usr/lib/libgfortran.so \
		"$subpkgdir"/usr/lib/
	if $_libquadmath; then
		mv "$pkgdir"/usr/lib/libquadmath.a \
			"$pkgdir"/usr/lib/libquadmath.so \
			"$subpkgdir"/usr/lib/
	fi
	mv "$pkgdir"/$_gcclibdir/finclude "$subpkgdir"/$_gcclibdir/
	mv "$pkgdir"/$_gcclibexec/f951 "$subpkgdir"/$_gcclibexec
	mv "$pkgdir"/usr/lib/libgfortran.spec "$subpkgdir"/$_gcclibdir
}

libgnat() {
	pkgdesc="GNU Ada runtime shared libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgna*.so "$subpkgdir"/usr/lib/
}

gnat() {
	pkgdesc="Ada support for GCC"
	depends="gcc=$_gccrel"
	provides="$pkgname-gnat-bootstrap"
	[ "$CHOST" = "$CTARGET" ] && depends="$depends libgnat=$_gccrel"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/$_gcclibdir \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/$_gcclibexec/*gnat* "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibdir/*ada* "$subpkgdir"/$_gcclibdir/
	mv "$pkgdir"/usr/bin/*gnat* "$subpkgdir"/usr/bin/
}

sha512sums="42ae38928bd2e8183af445da34220964eb690b675b1892bbeb7cd5bb62be499011ec9a93397dba5e2fb681afadfc6f2767d03b9035b44ba9be807187ae6dc65e  gcc-10.2.0.tar.xz
5edc2dfe8e5d680c8c622b16943a1ec4e7f7db245e2fa4e54371962c048af1f074b1ed49f619f8abdf0eb97129d8b7198b4f16f06da794fe0fd6ce4d55f829fb  0001-posix_memalign.patch
bff1c9a08d32186b94660c42823babd63729b2a050e761c514c3f23c7eac2585ddc4f748df547915fb6b2fc098f3ecc2e8d9707f3aef9bb9c6103ba92ed5f906  0002-gcc-poison-system-directories.patch
7662cc8c6cd77a6cacb2899f4fb13d14495feb508bad9ee8ae0dd38ecddf8d45391ec636022913eece41661fbd5d336ac2130a5c08b2495eb5aff5501cff49a6  0003-Turn-on-Wl-z-relro-z-now-by-default.patch
26303e588f37c4208c3da962972b83759daa5abaf8eaf4da331a4cf1e3a90590ad56a7f8b766795bda7b6baa69aaa9c09864f297d376feacbe4ba08249da2d8d  0004-Turn-on-D_FORTIFY_SOURCE-2-by-default-for-C-C-ObjC-O.patch
4be2f8ab07e5ff2b018c633e057927446259b63cc65229f977c84b294820d1dbecc29c2e6167b3cb000a5995e7def64cd8191031ee2b2c2ec3e5b4ad70267083  0005-On-linux-targets-pass-as-needed-by-default-to-the-li.patch
e97e619874c0edb60d826669426f96b6f09b79849538415ee1a205345a574ea292de5f324925dd1c821859a3c4873a2392e11edcd6ac6b53f6d6d389b25ffe94  0006-Enable-Wformat-and-Wformat-security-by-default.patch
c3081e7b37dcb2e2b4381e6fcc2abfa066bb4adc3ad4bdd7bd1402a6a3ccc354bfcf713c6acb379001dd97e14e6bdc8eb2eeca6b1e45262bea3262d51394c0e6  0007-Enable-Wtrampolines-by-default.patch
cc602c71b0893b058021cd45d239c3e9f013f68cd56f642fcdad342456bae9ea08ddf0929a6c2e6e452987d1d8c4be97dfe0314d9107e01bf6403ce97e83a30f  0008-Disable-ssp-on-nostdlib-nodefaultlibs-and-ffreestand.patch
0bc96d3a2afd0499e01b47a8197f9a5ead04219d0674f0dea97a4654def7dca2c50d8c005d04558212d53e4ec9fcf2bf4a3286d7a12b24eba0134aa1197abfe1  0009-Ensure-that-msgfmt-doesn-t-encounter-problems-during.patch
2662eadcc54408f5c6596e24615a6771c046575a53b8aec65677fcaa699446021b8f3360efc7660d72bf004bffd98f9d4f9d64eba97dedcbb752bb3fffc5a048  0010-Don-t-declare-asprintf-if-defined-as-a-macro.patch
0003e594bbbf142d678f0f79fc03ff467276331a8c87016ecd5f9683abb14aea2550e6880995dd160dc92a3e4ec04e4b2323eda403d4fa8453c258c28bcc969e  0011-libiberty-copy-PIC-objects-during-build-process.patch
209f7cfb281c3c10af2b84e1542e9093bb0fb36171c7d12ce33b1d92a70388d0aae7b1bb22a613a59e352de62927a0f8e9d3a0b4729c12f8eebacc74d29ecfdd  0012-libitm-disable-FORTIFY.patch
e72ad6130dd5dd64ef34ff9f07cf23d310d0d530b272fa1bdc9459991dfb10eec64af3249f213d7f4393024d7d5111890f74679e1029ee0e97b1b6c12a708296  0013-libgcc_s.patch
16fa64460806a6456c8185cf703e7afdf87f55cf49256ed06fd1c098d97d69a3d1df715bb409984ebd275009495ac9ee01bc090580558d2ff569fa2dbbff5fa6  0014-nopie.patch
e054c819446d58e119570cca88a23f541c215c8e4a6eb65d194b97610046593981aaa306d74d39ad6868cb2a85da510dedefd0b729804e3b88a518e4a0897d20  0015-libffi-use-__linux__-instead-of-__gnu_linux__-for-mu.patch
1b70eae1dfb7b819555c51c51d88b9acd09f44d491bf09b265bed7027721c3a74628715fd1bbc537ba43071e98e6747e08ad117b78fedda19571d7a8f6e2c9db  0016-dlang-update-zlib-binding.patch
9f4cf47a2f4016cb6808f245159d9365134963765af41654d60c2f62d4622405f1d1041c3690d50a2db9501075ccc932efd32c739bb0d241af5b80829cb8bafe  0017-dlang-fix-fcntl-on-mips-add-libucontext-dep.patch
55ca9cc19376ac4ce333abdee700e185811455419da80b0f6d765d079aa1344073d09883add920530ad62a3b9c01c347f634931f3735cb021b81b8fd7229467c  0018-ada-fix-shared-linking.patch
b461122d5bc5778398c72bc02bef6ee344ba6c79b7ff9945f9d252559ad445b298e154d7c4784e22fc5cf9d299fc3a8cfc3c0e4d37f12b961148e0432ac4e434  0019-build-fix-CXXFLAGS_FOR_BUILD-passing.patch
61fdf02497bc5d90bdc1c9b2aa0294bbe1ab4285940dc840d94bda00b0c4dec342df639b11721e5569a6de47e53099928f1f1fb07ddcf3466535e8092b2af9a7  0020-libstdc-futex-add-time64-compatibility.patch
b40fb4f96693a7a0cec086c24f17ba5c8c416e67d76d468306b26895033ee326454a2b2d3028077b9f2187c545bff8c99e87d44298c179ccbc1f48f04ccc0889  0021-add-fortify-headers-paths.patch
7a7414d44a43260d7098bf24dacea6595fdaf42496ef1d176bb7a254da813c1faaf1ecfa6e1463af34c466f02cf2f8489dbfeae9bbfb070726d27da0330e4c59  0022-Alpine-musl-package-provides-libssp_nonshared.a.-We-.patch
7bd3b297a05df00a0c961c407d84c60b7dcf5150919a6ce8e6d5162bb82e5820ae938feec24dad62dca141b4ede302345b6e08e21f06dd9393bec7592a8aecf7  0023-DP-Use-push-state-pop-state-for-gold-as-well-when-li.patch
5c61dac5100dce45ad17f45e5e63e3f1e46173639384d7fb3c343e0b6a1e4c0ab39491d397c4445be63cf182f3bb79f7995cc8f89f70a61321dea24bf29cdd18  0024-Pure-64-bit-MIPS.patch
be8eee9edfe4e768ea4873b618a3431ae4502e933af343f80b623e38626c0e9187b73debd6773ae8dfbfb514ff0e4a18a5102e5fac4545e5de18f8e226e6b237  0025-use-pure-64-bit-configuration-where-appropriate.patch
2a2c89b3dfac37344bc17fbb9471b903cbdf4332c71bb20942587acccf962a42d58d2535cc0d90a0f6cddc8f6db1328c6b9f158aecbff99a92f9fa0e0683c0a7  0026-always-build-libgcc_eh.a.patch
739e9d58875c291af82ca9d7509f52679a01704beb4f003e3ace1cd1ae2aede5763243e34c2c73be13bd1dd46539f50d3b7c7a293686b0c6158d3069d377009e  0027-ada-libgnarl-compatibility-for-musl.patch
69f330f91a3354753e9283e804f18257c3e7a51f368defe4a704d20c2a00fd6d6cb62f4d7931ca886dd7dffeaf59fa9beaf36c19ebd3d25cdebfefc884d64c02  0028-ada-musl-support-fixes.patch"
