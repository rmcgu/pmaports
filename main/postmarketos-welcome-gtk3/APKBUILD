# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
pkgname=postmarketos-welcome-gtk3
pkgver=0.2.0
pkgrel=0
pkgdesc="postmarketOS welcome app"
url="https://gitlab.com/postmarketOS/postmarketos-welcome-gtk3"
arch="all"
license="GPL-3.0-or-later"
makedepends="meson libhandy-dev"
source="https://gitlab.com/postmarketOS/postmarketos-welcome-gtk3/-/archive/$pkgver/postmarketos-welcome-gtk3-$pkgver.tar.bz2"
options="!check" # No tests

build() {
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=plain \
		. output
	ninja -C output
}

package() {
	DESTDIR="$pkgdir" ninja -C output install

	install -Dm644 "data/welcome-message" \
		"$pkgdir/etc/welcome-message"

	# Autostart on first login, the app removes the symlink when started
	local _autostartdir="$pkgdir/etc/skel/.config/autostart"
	mkdir -p "$_autostartdir"
	ln -s "/usr/share/applications/org.postmarketos.Welcome.desktop" \
		"$_autostartdir/org.postmarketos.Welcome.desktop"
}

sha512sums="a975e33bcd9520316dd83c36293f5e2a8945781f4f3e3b80a13bc7016e3757dbefc2e98af31c21bf7231d0fc20e82fe8555a5c3352a37348099f7007f52d9c72  postmarketos-welcome-gtk3-0.2.0.tar.bz2"
